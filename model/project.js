import mongoose from "mongoose";

const ProjectSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Post title is requried!"],
    },
    address: {
      type: String,
      required: [true, "Link address is requried!"],
    },
    description: {
      type: String,
      required: [true, "Project description is required"],
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);

export default mongoose.model("Project", ProjectSchema);
