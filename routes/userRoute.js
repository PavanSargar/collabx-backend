import express from "express";
const router = express.Router();

import isAuthenticated from "../middleware/isAuthenticated.js";
import projectByUser from "../controller/userController.js";

router.post("/find/projects/:id", isAuthenticated, projectByUser);

export default router;
