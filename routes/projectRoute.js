import express from "express";
const router = new express.Router();

import isAuthenticated from "../middleware/isAuthenticated.js";
import { create, userByProject, projectList } from "../controller/projectController.js";

router.post("/create/:id", create);
router.post("/populate/:id", isAuthenticated, userByProject);
router.get("/list", projectList)

export default router;
