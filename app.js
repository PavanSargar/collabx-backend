import * as dotenv from "dotenv";
import express from "express";
import mongoose from "mongoose";
import morgan from "morgan";
import cookieParser from "cookie-parser";
import cors from "cors";
import cluster from "cluster";
import os from "os";

dotenv.config();
const app = express();
const numCPUs = os.cpus().length;
const PORT = process.env.PORT || 5000;

// MIDDLEWARES
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(
  cors({
    credentials: true,
    optionsSuccessStatus: 200,
    origin: ["http://localhost:3000", "https://collabx.netlify.app"],
  })
);

// ROUTES
import userRoute from "./routes/userRoute.js";
import authRoute from "./routes/authRoute.js";
import projectRoute from "./routes/projectRoute.js";

const DBURI = process.env.DBURL;

const dbConnect = async () => {
  await mongoose
    .connect(DBURI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    })
    .then(() => console.log("🔗 db connected..."))
    .catch((error) => console.error(error));
};

dbConnect();

// USING ROUTE
app.use("/api/user", userRoute);
app.use("/api/auth", authRoute);
app.use("/api/project", projectRoute);

// SINGLE CORE UTILIZATION

if (process.argv[2] === "prod") {
  if (cluster.isPrimary) {
    console.log(`Primary ${process.pid} is running`);
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    cluster.on("exit", (worker, code, signal) => {
      console.log(`worker ${worker.process.pid} died`);
      cluster.fork();
    });
  } else {
    app.listen(PORT, () => {
      console.log(`🚀 server @ ${PORT} with ${process.pid} `);
    });
  }
} else {
  app.listen(PORT, () => {
    console.log(`🚀 Server is listening on port ${PORT}...`);
  });
}
