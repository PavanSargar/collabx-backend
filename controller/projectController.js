import Project from "../model/project.js";
import User from "../model/user.js";

export const create = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, description, address } = req.body;
    if (!title || !description || !address) {
      return res.status(500).send("invalid data")
    }
    const newProject = await Project.create({
      title,
      address,
      description,
      user: id,
    });

    await newProject.save();

    const userById = await User.findById(id);
    console.log("found user:", userById);

    userById.projects.push(newProject);
    await userById.save();

    return res.send(userById);
  } catch (error) {
    return res.status(500).send(error);
  }
};

export const userByProject = async (req, res) => {
  const { id } = req.params;
  const userByProject = await Project.findById(id).populate("user");
  res.send(userByProject);
};

export const projectList = async (req, res) => {
  const projects = await Project.find().populate("user");

  res.send(projects);
};
