import User from "../model/user.js";
import Crypto from "crypto";
import catchAsync from "../utils/catchAsync.js";

// exports.update = catchAsync(async (req, res) => {});

const projectByUser = async (req, res) => {
  const { id } = req.params;
  const user = await User.findById(id).populate("projects");

  res.send(user);
};

export default projectByUser;
